# Anomura 
> Design created and developed by Aristarh Bahirev & CalmiraLinux team 

## What is Anomura?
- Anomura is a minimalist desktop environment based on the Wayland protocol and the sway window manager 
- Extremely lightweight compared to larger DEs like KDE or Gnome, but is ready to go after installation
- Developed under the Calmira GNU/Linux(-libre) project, also available under Fedora via manual install or [script](./tools/fedora/anomura.sh) 

![anomura screenshot](./screenshots/anomura.png)

## Installation
1. To install Anomura along with Calmira GNU/Linux(-libre), you need to go to the official [web application]() in the `Download` tab, click on `Download Extended edition`, after downloading the iso image, follow the instructions inside
2. (IN DEVELOPMENT) To install on Fedora, run the following commands:
```sh
# For the script to work correctly, the system must be installed using [network installer](https://download.fedoraproject.org/pub/fedora/linux/releases/37/Everything/x86_64/iso/Fedora-Everything-netinst-x86_64-37-1.7.iso), 
# with the profile xorg + btrfs + pipewire + linux (vanilla)

sudo dnf update
sudo dnf upgrade
sudo dnf install git
sh -c "$(curl -fsSL https://gitlab.com/calmiralinux/anomura/tools/fedora/anomura.sh)"

## Software
- All software that is used in Anomura to build the desktop environment is in the file [software.md](./docs/software.md),where you can find both the name of programs, utilities, etc., but also their brief description
- Installing custom software can be done in 2 ways:
   1. assembly from `cports`
   2. installation of `AppImage` with further integration via [`AppImage Installer`](https://gitlab.com/calmiralinux/appimage-installer) (recommended)

## Appearance
The appearance and its settings are described in the [appearance.md](./docs/appearance.md) file, exactly those components that are specified there come by default with the system. If you wish, you can change them to those that you like

## Wallpapers
The following desktop wallpapers are used in the working environment:
- From the KDE project - Cold Ripple by [Risto Saukonpää](https://github.com/KDE/plasma-workspace-wallpapers/tree/master/ColdRipple)
- From our friend and wonderful photographer - [katze_942](https://www.flickr.com/photos/195282637@N07/) 

## License
Distributed under the Apache-2.0 License

## Contact us
If you are learning or actively earning on [shell scripting](https://www.freecodecamp.org/news/shell-scripting-crash-course-how-to-write-bash-scripts-in-linux/), [css](https://www.w3schools.com/css/default.asp) then write us an [telegram](https://t.me/calmira_gnu_linux)
