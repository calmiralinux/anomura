# Appearance

- Papirus - [icons theme](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme)
> It is the largest constantly developing project in the collection of which there is a huge number of beautiful and convenient icons for many

- Capitaine - [cursors theme](https://github.com/keeferrourke/capitaine-cursors)
> Cursor theme based on KDE Breeze

- Qogir - [GTK 2 and GTK 3 theme](https://github.com/vinceliuice/Qogir-theme)
> This is a flat design theme for GTK 2 and GTK 3


## ATTENTION! THIS COMPONENT IS NOT READY TO USE!
- Kvantum Qogir - [Qt theme](https://github.com/vinceliuice/Qogir-kde)
> This component is responsible for displaying and controlling the application theme on Qt 5

