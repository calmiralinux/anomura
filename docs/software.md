# Software
The set of software to get started and learn Anomura on Fedora and Calmira GNU/Linux(-libre)
> You can install all the components yourself and customize as you see fit

## The basis for the working environment
1. wayland - [window system and its protocol, X.org replacement](https://gitlab.freedesktop.org/wayland/wayland)
2. xwayland - [X.org server running on top of the Wayland server](https://github.com/timon37/xwayland)
3. x.org - ...
4. wlroots - [wayland compositor](https://github.com/swaywm/wlroots)
5. sway - [windows manager](https://github.com/swaywm/sway)
6. waybar - [panel](https://github.com/Alexays/Waybar)
7. lightdm - [display manager](https://github.com/canonical/lightdm)
8. pipewire - [sound subsystem](https://github.com/PipeWire/pipewire)
9. libinput - [input library](https://github.com/wayland-project/libinput)

## Additional software and components to help build the system
1. swaybg - [wallpapers control](https://github.com/swaywm/swaybg)
2. swaylock - [screen locker](https://github.com/swaywm/swaylock)
3. swayidle - [idle manager](https://github.com/swaywm/swayidle)
4. wl-clipboard - [terminal clipboard, xclip replacement](https://github.com/bugaevc/wl-clipboard)
5. git - [distributed version control system](https://github.com/git/git)
6. curl - [data transfer and download utility](https://github.com/curl/curl)
7. bash - [Unix shell and command interpreter](https://github.com/bminor/bash)
8. appimage installer - [helper for installing, running and integrating AppImages](https://gitlab.com/calmiralinux/appimage-installer)

## Fonts and themes for system and terminal
1. noto-fonts - [default system font](https://github.com/googlefonts/noto-fonts)
2. noto-fonts-emoji - [font that supports the Unicode emoji specification](https://github.com/googlefonts/noto-emoji)
3. liberation-fonts - [open source analogue of commercial fonts Arial, Times New Roman and Courier New](https://github.com/liberationfonts/liberation-fonts)
4. otf-font-awesome - [iconic SVG and font](https://github.com/FortAwesome/Font-Awesome)
5. fira-code - [default terminal and panel font](https://github.com/tonsky/FiraCode) 

## Software for the user
1. nvim - [text editor](https://github.com/neovim/neovim)
> For building a fast and functional IDE based on nvim, I recommend the [AstroNvim](https://github.com/AstroNvim/AstroNvim) project
2. midnight commander (mc) - [file mananger](https://github.com/MidnightCommander/mc)
3. firefox - [web browser](https://www.mozilla.org/en/firefox/linux/)
4. foot - [terminal](https://github.com/DanteAlighierin/foot)
5. slurp - [selection of an arbitrary area of the screen](https://github.com/emersion/slurp)
6. grim - [screenshot tool](https://github.com/emersion/grim)
7. pavucontrol - [volume control utility](https://github.com/pulseaudio/pavucontrol)
8. gsimplecal - [calendar](https://github.com/dmedvinsky/gsimplecal)
9. zathura - [document viewer](https://github.com/pwmt/zathura)
10. cmus - [music player](https://github.com/cmus/cmus)
11. mpv - [video player](https://github.com/mpv-player/mpv) 
12. imv - [image viewer](https://github.com/eXeC64/imv)
